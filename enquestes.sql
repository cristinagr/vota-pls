-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Temps de generació: 13-04-2018 a les 10:37:39
-- Versió del servidor: 5.7.21-0ubuntu0.16.04.1
-- Versió de PHP: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

create database enquestes;
use enquestes;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `enquestes`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `enquesta`
--

CREATE TABLE `enquesta` (
  `id` int(10) UNSIGNED NOT NULL,
  `pregunta` varchar(250) CHARACTER SET latin1 NOT NULL,
  `data_inici` date NOT NULL,
  `data_final` date NOT NULL,
  `destacada` tinyint(1) NOT NULL DEFAULT '0',
  `tema` varchar(15) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Bolcant dades de la taula `enquesta`
--

INSERT INTO `enquesta` (`id`, `pregunta`, `data_inici`, `data_final`, `destacada`, `tema`) VALUES
(1, 'Mudkips?', '2018-03-16', '2018-03-17', 1, 'Oci'),
(2, 'Consideres que a Catalunya hi ha presos politics?', '2018-03-20', '2018-04-10', 1, 'Politica'),
(3, 'Pregunta?', '2018-04-13', '2018-04-26', 0, 'Oci'),
(4, 'Pregunta?', '2018-04-13', '2018-04-26', 0, 'Oci'),
(5, 'Si o No ?', '2018-04-12', '2018-04-19', 0, 'Oci');

-- --------------------------------------------------------

--
-- Estructura de la taula `resposta`
--

CREATE TABLE `resposta` (
  `id_usuari` int(10) NOT NULL,
  `id_enquesta` int(10) NOT NULL,
  `valor` tinyint(1) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Bolcant dades de la taula `resposta`
--

INSERT INTO `resposta` (`id_usuari`, `id_enquesta`, `valor`, `data`) VALUES
(3, 1, 0, '2018-03-15 23:00:00'),
(3, 2, 0, '2018-03-20 23:00:00'),
(4, 2, 1, '2018-03-24 23:00:00'),
(4, 1, 0, '2018-03-16 23:00:00');

-- --------------------------------------------------------

--
-- Estructura de la taula `usuari`
--

CREATE TABLE `usuari` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(25) CHARACTER SET latin1 NOT NULL,
  `password` binary(10) NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Bolcant dades de la taula `usuari`
--

INSERT INTO `usuari` (`id`, `username`, `password`, `admin`) VALUES
(2, 'usuari1', 0x31323334356162636465, 1),
(3, 'usuari2', 0x31323334353661730000, 0),
(4, 'usuari3', 0x31323132313231320000, 0);

--
-- Indexos per taules bolcades
--

--
-- Index de la taula `enquesta`
--
ALTER TABLE `enquesta`
  ADD PRIMARY KEY (`id`);

--
-- Index de la taula `resposta`
--
ALTER TABLE `resposta`
  ADD PRIMARY KEY (`id_usuari`,`id_enquesta`);

--
-- Index de la taula `usuari`
--
ALTER TABLE `usuari`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `enquesta`
--
ALTER TABLE `enquesta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la taula `usuari`
--
ALTER TABLE `usuari`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
