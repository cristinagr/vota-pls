$(function() {
	"use strict";

	// aixo es el que rebras del php de la cristina, el defineixo en hard per poder fer els proves
	let enquestes = [{
		"id":"1",
		"pregunta":"Mudkips?",
		"data_inici":"2018-03-16",
		"data_final":"2018-03-17",
		"destacada":"1",
		"tema":"Oci"
	},
	{
		"id":"2",
		"pregunta":"Consideres que a Catalunya hi ha presos politics?",
		"data_inici":"2018-03-20",
		"data_final":"2018-04-10",
		"destacada":"1",
		"tema":"Politica"
	},
	{
		"id":"3",
		"pregunta":"Pregunta?",
		"data_inici":"2018-04-13",
		"data_final":"2018-04-26",
		"destacada":"0",
		"tema":"Oci"
	},
	{
		"id":"4",
		"pregunta":"Pregunta?",
		"data_inici":"2018-04-13",
		"data_final":"2018-04-26",
		"destacada":"0",
		"tema":"Oci"
	}];
	let template;

	let data = {
	};

	let model = { //Te les funcions 
		init: function() {
            $("#contestar").on("click",function(){
              view.mostraEnquestes();   
        
			});
			// en quant fagi click en qualsevol boto executara aquest codi
			$('button').on('click', function() {
				// switch de la id. Es a dir, gestionem que fara cada boto segons el nom de la id
				switch($(this).get(0).id) {
					case 'enquestes':
						// per cada enquesta executara el "view.mostraEnquesta" li passem el "value" que conte les enquestes individualment
						$.each(enquestes, function(key, value){
							view.mostraEnquesta(value)
						});
						break;
					case 'destacades':
						$.each(enquestes, function(key, value){
							// el mateix que l'anterior pero nomes executa si l'enquesta esta destacada (te el valor de destacada a 1)
							if(value.destacada == 1) {
								view.mostraEnquesta(value)
							}
						});
						break;
				}
			});
			//$('#enquestes').html("Hola bon dia visca jaja <h1>La cosa ara es que faci algo més</h1>");
			template = $.ajax({type: "GET", url: "template/enquestes_respondre.html", async: false }).responseText;

		}
	};

	let controller = { //Executa les funcions

		init: function() {
			model.init();
			view.init();
		}
	};


	let view = { //Com afecta a la pàgina (el que es veurà)
		init: function() {

        },
		// mostraEnquestes reb les enquestes individualment
		mostraEnquestes: function(enquesta) {
			// per cada enquesta passem per cadascun dels seus valors
			$.each(enquesta, function(key, value) {
				// aqui mostrem els valors, demoment per consola
				console.log(value);

				//console.log(template);
				let enquesta = template;
				
					let search = "{{ " + value + " }}";
					let valor = "Funciona";
					enquesta = enquesta.replace(search, valor);
				
				
				$("#enquestes").html(enquesta);	
			});
		}
	

	};

	$('#destacades').on('click',function(){
		console.log("HEY!");
	});

	controller.init();
});


