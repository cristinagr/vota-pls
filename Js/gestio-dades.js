// Funcio abreviada de jQuery per iniciar en quant carregui la pagina
$(function() {
		
	let enquestes;
	// Server JSON
	// agafem la informacio del php creat via ajax
		$.getJSON( "php/json.php", function() { // indiquem la ruta del php aqui
		// success de la funcio
		console.log( "success" );
	})
	.done(function(response) {
		enquestes = response;
		// la funcio obte un resultat
		console.log("second success");
		mostrarChart();
	})
	.fail(function() {
		// la funcio no obte cap resultat, o surgeix algun error
		console.log( "error" );
	})
	
	.always(function() {
		// funcions que sempre fara tant si funciona com si no
		console.log( "complete" );
	});

	function mostrarChart(){
		$.each(enquestes, function(key, value){
		console.log(value);
		if(value.id == 1){
			var ctx = document.getElementById("myChart").getContext("2d"); 
			//var si = Select count(valor) from resposta where id_enquesta =value.id && valor ==0;
			//var no = Select count(valor) from resposta where id_enquesta =value.id && valor ==1;
			var myChart = new Chart(ctx, {
				type: 'pie',
				data: {
					labels: ["Sí", "no"],
					datasets: [{
					label: 'Number of Votes',
					data: [/*ys*/4,/*nope*/ 5],
					backgroundColor: ['rgba(255, 99, 132, 0.2)','rgba(54, 162, 235, 0.2)'],
					borderColor: ['rgba(255,99,132,1)','rgba(54, 162, 235, 1)'],
					borderWidth: 1
				}]},
			});}
		});
	
		}

});
