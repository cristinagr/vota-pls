function mostrarLogin() {
    var x = document.getElementById("form_login");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
function tancarLogin() {
    var x = document.getElementById("form_login");
    if (x.style.display === "block") {
        x.style.display = "none";
    } 
		else {
        x.style.display = "block";
    }
}

 function dadesIncorrectes() {
            swal({
              title: "Error", 
              text: "Usuari o contraseña incorrectes", 
              icon: "error",
              confirmButtonText: "Yes",
              confirmButtonColor: "#ec6c62"
            }); 
    }
    function correcte(){
            swal({
                title: "Todo ha salido a pedir de Millhouse", 
                icon: "success",
                confirmButtonText: "Yes",
                confirmButtonColor: "#ec6c62"
           }); 
   }

function destacada() {
    
}
function charts(){
	
	var ctx = document.getElementById("myChart").getContext("2d");

	//var si = Select count(valor) from resposta where id_enquesta ="X" && valor ==0;
	//var no = Select count(valor) from resposta where id_enquesta ="X" && valor ==1;

	var myChart = new Chart(ctx, {
		type: 'pie',
		data: {
			labels: ["Sí", "no"],
			datasets: [{
			label: '# of Votes',
			data: [4, 5],
			backgroundColor: ['rgba(255, 99, 132, 0.2)','rgba(54, 162, 235, 0.2)'],
			borderColor: ['rgba(255,99,132,1)','rgba(54, 162, 235, 1)'],
			borderWidth: 1
		}]
	},
	});
  
}
