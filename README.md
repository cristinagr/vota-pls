# Projecte Vota

#### Creat per:
	> Arnau Royo Raso
	> Pol Rodés Ribote
	> Cristina García Rodríguez
	
#### Objectiu “breu” del projecte

Creació d'una web en la que hi hagin enquestes, en la que els usuaris puguin crear enquestes i respondreles, apart de saber quines respostes hi ha hagut i quantes respostes tenen.

#### Estat “breu” del projecte

En funcionament i en estat inicial

##### Adreça web d’un full de calcul compartit (drive) on heu de posar per cada hora, la tasca que ha realitzat cada integrant del grup (model) 

 - https://docs.google.com/spreadsheets/d/1kKT3O3ynnvQDr4vne5eFe7sBWhCL-dVdpm50zwkDy3k/edit#gid=1003519625

##### Adreça web de la documentació phpdoc (labs.iam.cat)

- Encara no és existent.

##### Adreça web del projecte desplegat (labs.iam.cat)

 - No exisetix de moment
 
##### Mockup de la pàgina web
 - [Fet amb Moqup](https://app.moqups.com/a16crigarrod@iam.cat/KztkIekfT0/view/page/aa9df7b72)
