<?php
  require('login.php');
	include('header.php');
?>
  <body>
    <section>
      <center>
				<form method="POST" action="insertar_enquesta.php">
					<label for="pregunta">Pregunta de l'enquesta</label>
					<input type="text" id="pregunta" autocomplete="off" name="pregunta" placeholder="Pregunta amb resposta sí o no">
					<br>
					<label for="data_inici">Data inici</label>
					<input type="date" id="data_inici" name="data_inici">
					<br>
					<label for="data_final">Data fi</label>
					<input type="date" id="data_final" name="data_final">
					<br>
					<input type="checkbox" name="destacada" value="destacada">Enquesta destacada<br>
					<br>
					<label for="tema">Tema</label>
						<select name="tema" id="tema">
							<option value="Política">Política</option>
							<option value="Tecnología">Tecnología</option>
							<option value="Oci">Oci</option>
							<option value="Gastronomía">Gastronomía</option>
						</select>
					<input class="submit" type="submit" name="submit" value="Enviar" >
				</form>
      </center>
<?php	include('footer.php');?>