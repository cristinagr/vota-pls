<?php
  require('login.php');
	session_start();
?>
	<!DOCTYPE html>
	<html lang="cat">
	<!--Tots els fitxers a tenir en compte, amb els estils: milligram i normalize, i el propi.
Tamb� estan els javascripts/jquery-->

	<head>
		<meta charset="utf-8">
		<title>Vota! IAM</title>
		<script type="text/javascript" src="/Js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/css/litera.css">
		<link rel="stylesheet" type="text/css" href="/css/vota.css">
		<script type="text/javascript" src="/Js/Chart.min.js"></script>
		<script type="text/javascript" src="/Js/gestio-dades.js"></script>
		<script type="text/javascript" src="/Js/enquesta.js"></script>
		<script type="text/javascript" src="/assets/js/custom.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<!-- Favicon -->
		<link rel="shortcut icon" href="/assets/img/favicon.png">
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
		<!-- CSS Global Compulsory -->
		<link rel="stylesheet" href="/assets/vendor/bootstrap/bootstrap.min.css">
		<!-- CSS Unify -->
		<link rel="stylesheet" href="/assets/css/unify-core.css">
		<link rel="stylesheet" href="/assets/css/unify-components.css">
		<link rel="stylesheet" href="/assets/css/unify-globals.css">
		<link rel="stylesheet" href="/assets/vendor/cubeportfolio-full/cubeportfolio/css/cubeportfolio.min.css">
		<!-- CSS Customization -->
		<link rel="stylesheet" href="/assets/css/custom.css">

	</head>

	<body>
		<!DOCTYPE html>
		<html lang="es">

		<head>
			<title>Vota!</title>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<meta http-equiv="x-ua-compatible" content="ie=edge">

			<!-- Favicon -->
			<link rel="shortcut icon" href="/assets/img/favicon.png">
			<!-- Google Fonts -->
			<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
			<!-- CSS Global Compulsory -->
			<link rel="stylesheet" href="/assets/vendor/bootstrap/bootstrap.min.css">
			<!-- CSS Unify -->
			<link rel="stylesheet" href="/assets/css/unify-core.css">
			<link rel="stylesheet" href="/assets/css/unify-components.css">
			<link rel="stylesheet" href="/assets/css/unify-globals.css">
			<link rel="stylesheet" href="/assets/vendor/cubeportfolio-full/cubeportfolio/css/cubeportfolio.min.css">

			<!-- CSS Customization -->
			<link rel="stylesheet" href="/assets/css/custom.css">
		</head>

		<body>
			<main>
				<!-- Header -->
				<header id="js-header" class="u-header u-header--static">
					<div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10">
						<nav class="navbar navbar-expand-lg hs-menu-initialized hs-menu-horizontal">
							<div class="container">
								<!-- Logo -->
								<a href="/" class="navbar-brand">
              <img class="g-width-150 g-height-auto" src="/assets/img/logo-1.png" alt="Logo Vota!">
            </a>
								<!-- End Logo -->
								<div class="d-inline-block g-hidden-xs-down g-pos-rel g-valign-middle g-pl-30 g-pl-0--lg">
									<a onclick="mostrarLogin()" class="btn u-btn-outline-primary g-font-size-13 text-uppercase g-py-10 g-px-15" action="" target="_blank">LOG IN</a>
									<a href="php/admin.php" class="btn u-btn-outline-primary g-font-size-13 text-uppercase g-py-10 g-px-15" action="" target="_blank">Admin</a>
								</div>
							</div>
						</nav>
					</div>
					<center id="form_login">
						<form method="post" action="" onsubmit="return do_login();">
							<img src="/assets/img/X.png" alt="X per a tancar" style="position:absolute;width:8%;left:0;border:1px solid black;background-color:white;border-radius:20px;padding:1%;cursor:pointer;" onclick="tancarLogin()">
							<label for="usuari">Nom d'usuari</label>
							<input type="text" name="User" title="usuari">
							<br>
							<label for="contrasenya">Contrasenya</label>
							<input type="password" name="User" value="">
							<br><input type="submit" name="login" value="Conecta't" id="login_btn">
							<br>No tens un compte?
							<br>Fes click aqui per a crear-lo <a href="https://www.google.es">Registra't</a>
							<!--En la href: posar la pag per a registrar-se si es fa. -->
						</form>
					</center>
				</header>