<?php
	// Força la desactivacio de la cache
	header('Cache-Control: no-cache, must-revalidate');
	header('Expires: Mon, 01 Jan 2016 00:00:00 GMT');
	// Defineix el tipus de dades
	header('Content-type: application/json; charset=UTF-8');
	// Database config

	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "enquestes";

	// Crea la conexio a la base de dades

	$conn = new mysqli($servername, $username, $password, $dbname);

	// Comprova la conexio
	
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}

	// Fa les consultes

	$sql = "SELECT * FROM enquesta"; /* agafa tot el de la taula */
	$result = $conn->query($sql);
	// Comprova que hi hagin resultats

	if ($result->num_rows > 0) {

		// definim l'output com una array buida i inserim les dades obtingudes dins d'aquesta
		$outp = array();
		$outp = $result->fetch_all(MYSQLI_ASSOC);

		// converteix l'array en un json
		echo json_encode($outp);

	} else {
		echo "0 results";
	}

	// Tanca la conexio

	$conn->close();

?>