<!-- Footer -->
<footer class="container g-pt-100 g-pb-20">
  <div class="row">
    <div class="col-sm-6 col-lg-3 g-mb-30">
      <a class="d-inline-block mb-4" href="/">
            <img class="g-width-100 g-height-auto" src="/assets/img/logo-1.png" alt="Logo Vota!">
          </a>

      <p class="g-color-gray-dark-v4 g-font-size-13">2018 &copy; Tots els drets, per qui els vulgui.</p>
    </div>

    <div class="col-sm-6 col-lg-3 g-mb-30">
      <h3 class="h6 g-color-black g-font-weight-600 text-uppercase mb-4">About</h3>

      <!-- Links -->
      <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-13">
        <li class="my-3"><i class="mr-2 fa fa-angle-right"></i>
          <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">Privacy &amp; Policy</a>
        </li>
        <li class="my-3"><i class="mr-2 fa fa-angle-right"></i>
          <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">Terms &amp; Conditions</a>
        </li>
      </ul>
      <!-- End Links -->
    </div>

    <div class="col-sm-6 col-lg-3 g-mb-30">
      <h3 class="h6 g-color-black g-font-weight-600 text-uppercase mb-4">Contacts</h3>

      <!-- Links -->
      <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-13">
        <li class="media mb-4">
          <i class="d-flex mt-1 mr-3 icon-hotel-restaurant-235 u-line-icon-pro"></i>
          <div class="media-body">
            <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="https://goo.gl/maps/3LiAvRLSTuP2">
                  Av. Espluges 38, 08034
                <br>Barcelona</a>
          </div>
        </li>
        <li class="media mb-4">
          <i class="d-flex mt-1 mr-3 icon-communication-062 u-line-icon-pro"></i>
          <div class="media-body">
            <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">ermengolvota@support.com</a>
          </div>
        </li>
        <li class="media mb-4">
          <i class="d-flex mt-1 mr-3 icon-communication-033 u-line-icon-pro"></i>
          <div class="media-body">
            +34 123 456 789
          </div>
        </li>
      </ul>
      <!-- End Links -->
    </div>

    <div class="col-sm-6 col-lg-3 g-mb-30">
      <h3 class="h6 g-color-black g-font-weight-600 text-uppercase mb-4">Follow Us</h3>

      <!-- Links -->
      <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-13">
        <li class="my-3"><i class="mr-2 fa fa-angle-right"></i>
          <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" target="_blank" href="https://ca-es.facebook.com/">Facebook</a>
        </li>
        <li class="my-3"><i class="mr-2 fa fa-angle-right"></i>
          <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" target="_blank" href="https://twitter.com/">Twitter</a>
        </li>
        </li>
        <li class="my-3"><i class="mr-2 fa fa-angle-right"></i>
          <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" target="_blank" href="https://www.linkedin.com/">LinkedIn</a>
        </li>
      </ul>
      <!-- End Links -->
    </div>
  </div>
</footer>
<!-- End Footer -->

<a class="js-go-to u-go-to-v1" href="#!" data-type="fixed" data-position='{
     "bottom": 15,
     "right": 15
   }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
   <img src="/assets/img/top.png" alt="Botó d'anar cap d'alt de tot" class="g-max-width-100x">
    </a>
</main>

<div class="u-outer-spaces-helper"></div>


<!-- JS Global Compulsory -->

<script src="/assets/vendor/jquery/jquery.min.js"></script>
<script src="/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>


<!-- JS Implementing Plugins -->
<script src="/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
<script src="/assets/vendor/cubeportfolio-full/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>


<!-- JS Unify -->
<script src="/assets/js/hs.core.js"></script>
<script src="/assets/js/components/hs.go-to.js"></script>
<script src="/assets/js/components/hs.cubeportfolio.js"></script>


<!-- JS Customization -->
<script src="/assets/js/custom.js"></script>

<!-- JS Plugins Init. -->
<script>
  $(document).on('ready', function() {
    // inicialització del botó que porta a enquestes destacades
    $.HSCore.components.HSGoTo.init('.js-go-to');
  });

  $(window).on('load', function() {
    // initialization of cubeportfolio
    $.HSCore.components.HSCubeportfolio.init('.cbp');
  });
</script>
</body>

</html>