<?php  
include('php/header.php');
	if(isset($_POST['logout'])){
		unset($_SESSION['username']);
	}
?>

<body>

	<!-- Promo Block -->
	<section class="g-bg-img-hero parallax" style="background-image: url(./assets/img/bg/happy.jpg);">
		<div class="container u-bg-overlay__inner text-center g-pt-150 g-pb-70 imagehappy">
			<div class="g-mb-100 h1-title">
				<h1 class="g-color-white  g-font-weight-700 g-font-size-120 g-font-size-120--md text-uppercase g-line-height-1_2 g-letter-spacing-10 mb-4">VOTA!</h1>
				<span class="d-block lead g-color-white g-letter-spacing-2 g-font-size-30">Crea, comparteix i diverteix-te</span>
			</div>

			<div class="g-pos-rel g-left-0 g-right-0 g-z-index-2 g-bottom-30">

				<a id="destacades" class=" js-go-to btn u-btn-outline-white g-color-white g-bg-white-opacity-0_1 g-color-black--hover g-bg-white--hover g-font-weight-600 text-uppercase g-rounded-50 g-px-30 g-py-11" data-target="#enquestes">
            <i>Enquestes Destacades</i>
          </a>

				<a id="contestar" class=" js-go-to btn u-btn-outline-white g-color-white g-bg-white-opacity-0_1 g-color-black--hover g-bg-white--hover g-font-weight-600 text-uppercase g-rounded-50 g-px-30 g-py-11" data-target="#enquestes">
            <i>Contestar Enquestes</i>
          </a>

			</div>
		</div>
	</section>
	<!-- End Foto Inicial amb Parallax -->
	<!-- Enquestes Destacades (Contenidor) -->
	<section id="enquestes" class="container g-py-100">


		<!-- Enquestes Destacades Filtre-->
		<ul id="filterControls1" class="d-block list-inline g-mb-50">
			<li class="list-inline-item cbp-filter-item g-color-iamblue g-color-iamorange--hover g-color-gray-dark-v5--active g-font-weight-600 g-font-size-13 text-uppercase pr-2 mb-2" role="button" data-filter="*">
				Tots
			</li>
			<li class="list-inline-item cbp-filter-item g-color-iamblue g-color-iamorange--hover g-color-gray-dark-v5--active g-font-weight-600 g-font-size-13 text-uppercase px-2 mb-2" role="button" data-filter=".identity">
				Política
			</li>
			<li class="list-inline-item cbp-filter-item g-color-iamblue g-color-iamorange--hover g-color-gray-dark-v5--active g-font-weight-600 g-font-size-13 text-uppercase px-2 mb-2" role="button" data-filter=".design">
				Tecnología
			</li>
			<li class="list-inline-item cbp-filter-item g-color-iamblue g-color-iamorange--hover g-color-gray-dark-v5--active g-font-weight-600 g-font-size-13 text-uppercase px-2 mb-2" role="button" data-filter=".graphic">
				Oci
			</li>
			<li class="list-inline-item cbp-filter-item g-color-iamblue g-color-iamorange--hover g-color-gray-dark-v5--active g-font-weight-600 g-font-size-13 text-uppercase px-2 mb-2" role="button" data-filter=".graphic">
				Gastronomía
			</li>
		</ul>
		<!-- End Filtre -->

		<!-- Enquestes Destacades (Contingut) -->
		<div class="cbp" data-controls="#filterControls1" data-animation="quicksand" data-x-gap="30" data-y-gap="30" data-media-queries='[{"width": 1500, "cols": 3}, {"width": 1100, "cols": 3}, {"width": 800, "cols": 3}, {"width": 480, "cols": 2}, {"width": 300, "cols": 1}]'>
			<!-- Enquestes Destacades (Item) -->
			<div id="shortcode" class="cbp-item identity design">
				<div class="u-block-hover g-parent col-4">
					<canvas id="myChart7" max-width="150px" max-height="150px"></canvas>
					<script>
						var ctx = document.getElementById("myChart7").getContext("2d");
						//var si = Select count(valor) from resposta where id_enquesta ="X" && valor ==0;
						//var no = Select count(valor) from resposta where id_enquesta ="X" && valor ==1;
						var myChart = new Chart(ctx, {
							type: 'pie',
							data: {
								labels: ["Sí", "no"],
								datasets: [{
									label: '# of Votes',
									data: [ /*si*/ 20, /*no*/ 5],
									backgroundColor: ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
									borderColor: ['rgba(255,99,132,1)', 'rgba(54, 162, 235, 1)'],
									borderWidth: 1
								}]
							},
						});
					</script>
				</div>
				<div class="text-center g-pa-25">

					<h3 class="h5 g-color-black mb-1">U O no tens pulmó</h3>
					<p class="g-color-gray-dark-v4 mb-0">Kahoot in my ass</p>
				</div>
			</div>
			<!-- End Enquestes Destacades (Item) -->

			<!-- Enquestes Destacades (Item) -->
			<div class="cbp-item design">
				<div class="u-block-hover g-parent col-4">
					<canvas id="myChart6" max-width="150px" max-height="150px"></canvas>
					<script>
						var ctx = document.getElementById("myChart6").getContext("2d");
						//var si = Select count(valor) from resposta where id_enquesta ="X" && valor ==0;
						//var no = Select count(valor) from resposta where id_enquesta ="X" && valor ==1;
						var myChart = new Chart(ctx, {
							type: 'pie',
							data: {
								labels: ["Sí", "no"],
								datasets: [{
									label: '# of Votes',
									data: [ /*si*/ 14, /*no*/ 25],
									backgroundColor: ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
									borderColor: ['rgba(255,99,132,1)', 'rgba(54, 162, 235, 1)'],
									borderWidth: 1
								}]
							},
						});
					</script>
				</div>
				<div class="text-center g-pa-25">
					<h3 class="h5 g-color-black mb-1">Development</h3>
					<p class="g-color-gray-dark-v4 mb-0">Design</p>
				</div>
			</div>
			<!-- End Enquestes Destacades (Item)-->

			<!-- Enquestes Destacades (Item) -->
			<div class="cbp-item graphic identity">
				<div class="u-block-hover g-parent col-4">
					<canvas id="myChart5" max-width="150px" max-height="150px"></canvas>
					<script>
						var ctx = document.getElementById("myChart5").getContext("2d");
						//var si = Select count(valor) from resposta where id_enquesta ="X" && valor ==0;
						//var no = Select count(valor) from resposta where id_enquesta ="X" && valor ==1;
						var myChart = new Chart(ctx, {
							type: 'pie',
							data: {
								labels: ["Sí", "no"],
								datasets: [{
									label: '# of Votes',
									data: [ /*si*/ 24, /*no*/ 30],
									backgroundColor: ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
									borderColor: ['rgba(255,99,132,1)', 'rgba(54, 162, 235, 1)'],
									borderWidth: 1
								}]
							},
						});
					</script>
				</div>
				<div class="text-center g-pa-25">
					<h3 class="h5 g-color-black mb-1">Project planner</h3>
					<p class="g-color-gray-dark-v4 mb-0">Graphic, Identity</p>
				</div>
			</div>
			<!-- End Enquestes Destacades (Item) -->

			<!-- Enquestes Destacades (Item) -->
			<div class="cbp-item graphic">
				<div class="u-block-hover g-parent col-4">
					<canvas id="myChart2" max-width="150px" max-height="150px"></canvas>
					<script>
						var ctx = document.getElementById("myChart2").getContext("2d");
						//var si = Select count(valor) from resposta where id_enquesta ="X" && valor ==0;
						//var no = Select count(valor) from resposta where id_enquesta ="X" && valor ==1;
						var myChart = new Chart(ctx, {
							type: 'pie',
							data: {
								labels: ["Sí", "no"],
								datasets: [{
									label: '# of Votes',
									data: [ /*si*/ 34, /*no*/ 45],
									backgroundColor: ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
									borderColor: ['rgba(255,99,132,1)', 'rgba(54, 162, 235, 1)'],
									borderWidth: 1
								}]
							},
						});
					</script>
				</div>
				<div class="text-center g-pa-25">
					<h3 class="h5 g-color-black mb-1">Design</h3>
					<p class="g-color-gray-dark-v4 mb-0">Graphic</p>
				</div>
			</div>
			<!-- End Enquestes Destacades (Item) -->

			<!-- Enquestes Destacades (Item) -->
			<div class="cbp-item identity">
				<div class="u-block-hover g-parent col-4">
					<canvas id="myChart3" max-width="150px" max-height="150px"></canvas>
					<script>
						var ctx = document.getElementById("myChart3").getContext("2d");
						//var si = Select count(valor) from resposta where id_enquesta ="X" && valor ==0;
						//var no = Select count(valor) from resposta where id_enquesta ="X" && valor ==1;
						var myChart = new Chart(ctx, {
							type: 'pie',
							data: {
								labels: ["Sí", "no"],
								datasets: [{
									label: '# of Votes',
									data: [ /*si*/ 14, /*no*/ 51],
									backgroundColor: ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
									borderColor: ['rgba(255,99,132,1)', 'rgba(54, 162, 235, 1)'],
									borderWidth: 1
								}]
							},
						});
					</script>
				</div>
				<div class="text-center g-pa-25">
					<h3 class="h5 g-color-black mb-1">Creative agency</h3>
					<p class="g-color-gray-dark-v4 mb-0">Identity</p>
				</div>
			</div>
			<!-- End Enquestes Destacades (Item) -->

			<!-- Enquestes Destacades (Item) -->
			<div class="cbp-item graphic">
				<div class="u-block-hover g-parent col-4">
					<canvas id="myChart4" max-width="150px" max-height="150px"></canvas>
					<script>
						var ctx = document.getElementById("myChart4").getContext("2d");
						//var si = Select count(valor) from resposta where id_enquesta ="X" && valor ==0;
						//var no = Select count(valor) from resposta where id_enquesta ="X" && valor ==1;
						var myChart = new Chart(ctx, {
							type: 'pie',
							data: {
								labels: ["Sí", "no"],
								datasets: [{
									label: '# of Votes',
									data: [ /*si*/ 20, /*no*/ 25],
									backgroundColor: ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
									borderColor: ['rgba(255,99,132,1)', 'rgba(54, 162, 235, 1)'],
									borderWidth: 1
								}]
							},
						});
					</script>
				</div>
				<div class="text-center g-pa-25">
					<h3 class="h5 g-color-black mb-1">Production</h3>
					<p class="g-color-gray-dark-v4 mb-0">Graphic</p>
				</div>
			</div>
			<!-- End Enquestes Destacades (Item) -->
		</div>
		<!-- End Enquestes Destacades (Contingut) -->
	</section>


	<!-- End Enquestes Destacades Blocks -->
	<br>
	<?php include('php/footer.php');?>